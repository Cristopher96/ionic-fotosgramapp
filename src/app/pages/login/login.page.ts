import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IonSlides, NavController } from '@ionic/angular';
import { UserService } from '../../services/user.service';
import { UiServiceService } from '../../services/ui-service.service';
import { User } from '../../interfaces/interfaces';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @ViewChild('mainSlide', {static: true}) slides: IonSlides;

  loginUser = {
    email: 'escorciacristopher@gmail.com',
    password: 'Cescorci10'
  };

  registerUser: User = {
    email: 'test',
    password: '123456',
    name: 'Test',
    avatar: 'av-1.png'
  };

  constructor(
    private userService: UserService,
    private navCtrl: NavController,
    private uiService: UiServiceService
  ) { }

  ngOnInit() {
    this.slides.lockSwipes(true);
  }

  async login(fLogin: NgForm) {
    if (fLogin.invalid) { return; }
    const valid = await this.userService.login(this.loginUser.email, this.loginUser.password);
    if (valid) {
      // navigate to tabs
      this.navCtrl.navigateRoot('/main/tabs/tab1', {animated: true});
    } else {
      // show no login alert
      this.uiService.infoAlert('User & passwords are invalids');
    }
  }

  async register(fRegister: NgForm) {
    if (fRegister.invalid) { return; }
    const valid = await this.userService.createUser(this.registerUser);
    if (valid) {
      // navigate to tabs
      this.navCtrl.navigateRoot('/main/tabs/tab1', {animated: true});
    } else {
      // show no login alert
      this.uiService.infoAlert('Email already taken.');
    }
  }

  showLogin() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
    this.slides.lockSwipes(true);
  }

  showRegister() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(1);
    this.slides.lockSwipes(true);
  }

}
