import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { Post } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  posts: Post[] = [];
  enable = true;
  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.next();
    this.postsService.newPost.subscribe(post => {
      this.posts.unshift(post);
    });
  }

  refresh(e) {
    this.next(e, true);
    this.enable = true;
    this.posts = [];
  }

  next(e?, pull: boolean = false) {
    this.postsService.getPosts(pull).subscribe(resp => {
      this.posts.push(...resp.posts);
      if (e) {
        e.target.complete();
        if (resp.posts.length === 0) {
          this.enable = false;
        }
      }
    });
  }

}
