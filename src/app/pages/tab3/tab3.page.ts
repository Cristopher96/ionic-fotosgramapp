import { Component, OnInit } from '@angular/core';
import { User } from '../../interfaces/interfaces';
import { UserService } from '../../services/user.service';
import { NgForm } from '@angular/forms';
import { UiServiceService } from '../../services/ui-service.service';
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  user: User = {};

  constructor(
    private userService: UserService,
    private uiService: UiServiceService,
    private postSerivce: PostsService
  ) {}

  ngOnInit() {
    this.user = this.userService.getUser();
  }

  async update(fUpdate: NgForm) {
    if (fUpdate.invalid) { return; }
    const updated = await this.userService.updateUserInfo(this.user);
    if (updated) {
      this.uiService.presentToast('Info updated');
    } else {
      this.uiService.presentToast(`Couldn't update the info`);
    }
  }

  logout() {
    this.postSerivce.postsPage = 0;
    this.userService.logout();
  }
}
