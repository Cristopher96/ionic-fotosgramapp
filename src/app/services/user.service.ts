import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { User } from '../interfaces/interfaces';
import { NavController } from '@ionic/angular';
import { UiServiceService } from './ui-service.service';

const URL = environment.url;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  token: string = null;
  private user: User = {};

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private navCtrl: NavController,
    private uiService: UiServiceService
  ) {}

  login(email: string, password: string) {
    const data = {email, password};

    return new Promise(resolve => {
      this.http.post(`${URL}/user/login`, data).subscribe(async (resp) => {
        console.log(resp);
        // tslint:disable-next-line: no-string-literal
        if (resp['ok']) {
          // tslint:disable-next-line: no-string-literal
          await this.saveToken(resp['token']);
          resolve(true);
        } else {
          this.token = null;
          this.storage.clear();
          resolve(false);
          this.uiService.presentToast('LA promesa fue resulta falsa');
        }
      });
    });
  }

  logout() {
    this.token = null;
    this.user = null;
    this.storage.clear();
    this.navCtrl.navigateRoot('/login', {animated: true});
  }

  async saveToken(token: string) {
    this.token = token;
    await this.storage.set('token', token);
    await this.verifyToken();
  }

  createUser(user: User) {
    return new Promise(resolve => {
      this.http.post(`${URL}/user/create`, user).subscribe(async resp => {
        console.log(resp);
        // tslint:disable-next-line: no-string-literal
        if (resp['ok']) {
          // tslint:disable-next-line: no-string-literal
          await this.saveToken(resp['token']);
          resolve(true);
        } else {
          this.token = null;
          this.storage.clear();
          resolve(false);
        }
      });
    });
  }

  async getToken() {
    this.token = await this.storage.get('token') || null;
  }

  async verifyToken(): Promise<boolean> {
    await this.getToken();
    if (!this.token) {
      this.navCtrl.navigateRoot('/login');
      return Promise.resolve(false);
    }
    return new Promise<boolean>(resolve => {
      const headers = new HttpHeaders({
        'x-token': this.token
      });

      this.http.get(`${URL}/user/`, {headers}).subscribe( resp => {
        // tslint:disable-next-line: no-string-literal
        if (resp['ok']) {
          // tslint:disable-next-line: no-string-literal
          this.user = resp['user'];
          resolve(true);
        } else {
          this.navCtrl.navigateRoot('/login');
          resolve(false);
        }
      });
    });
  }

  getUser() {
    if (!this.user._id) {
      this.verifyToken();
    }
    return {...this.user};
  }

  updateUserInfo(user: User) {
    const headers = new HttpHeaders({
      'x-token': this.token
    });
    return new Promise(resolve => {
      this.http.post(`${URL}/user/update`, user, {headers}).subscribe(resp => {
      // tslint:disable-next-line: no-string-literal
      if (resp['ok']) {
        // tslint:disable-next-line: no-string-literal
        this.saveToken(resp['token']);
        resolve(true);
      } else {
        resolve(false);
      }
    });
    });

  }
}
