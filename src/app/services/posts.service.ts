import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { PostsResponse, Post } from '../interfaces/interfaces';
import { UserService } from './user.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

const URL = environment.url;

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  postsPage = 0;
  newPost = new EventEmitter<Post>();

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private fileTransfer: FileTransfer
  ) {}

  getPosts(pull: boolean = false) {
    if (pull) {
      this.postsPage = 0;
    }
    this.postsPage++;
    return this.http.get<PostsResponse>(`${URL}/posts/?page=${this.postsPage}`);
  }

  createPost(post) {
    const headers = new HttpHeaders({
      'x-token': this.userService.token
    });

    return new Promise(resolve => {
      this.http.post(`${URL}/posts`, post, {headers}).subscribe(resp => {
        // tslint:disable-next-line: no-string-literal
        this.newPost.emit(resp['post']);
        resolve(true);
      });
    });
  }

  uploadImg(img: string) {
    const options: FileUploadOptions = {
      fileKey: 'img',
      headers: {
        'x-token': this.userService.token
      }
    };

    const fileTransfer: FileTransferObject = this.fileTransfer.create();
    fileTransfer.upload(img, `${URL}/posts/upload`, options).then(data => {
      console.log(data);
    }).catch(err => {
      console.log('Upload error', err);
    });
  }
}
